module main

go 1.16

require (
	github.com/grafana/grafana-plugin-sdk-go v0.114.0
	github.com/grafana/grafana-starter-datasource-backend v0.0.0-20211018093506-a874f6fe4d10
)
