package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/instancemgmt"
	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
)

// Make sure SampleDatasource implements required interfaces. This is important to do
// since otherwise we will only get a not implemented error response from plugin in
// runtime. In this example datasource instance implements backend.QueryDataHandler,
// backend.CheckHealthHandler, backend.StreamHandler interfaces. Plugin should not
// implement all these interfaces - only those which are required for a particular task.
// For example if plugin does not need streaming functionality then you are free to remove
// methods that implement backend.StreamHandler. Implementing instancemgmt.InstanceDisposer
// is useful to clean up resources used by previous datasource instance when a new datasource
// instance created upon datasource settings changed.
var (
	_ backend.QueryDataHandler      = (*AppDynamicsDatasource)(nil)
	_ backend.CheckHealthHandler    = (*AppDynamicsDatasource)(nil)
	_ backend.StreamHandler         = (*AppDynamicsDatasource)(nil)
	_ instancemgmt.InstanceDisposer = (*AppDynamicsDatasource)(nil)
)

// NewSampleDatasource creates a new datasource instance.
func NewAppDynamicsDatasource(_ backend.DataSourceInstanceSettings) (instancemgmt.Instance, error) {
	return &AppDynamicsDatasource{}, nil
}

// SampleDatasource is an example datasource which can respond to data queries, reports
// its health and has streaming skills.
type AppDynamicsDatasource struct{}

// Dispose here tells plugin SDK that plugin wants to clean up resources when a new instance
// created. As soon as datasource settings change detected by SDK old datasource instance will
// be disposed and a new one will be created using NewSampleDatasource factory function.
func (d *AppDynamicsDatasource) Dispose() {
	// Clean up datasource instance resources.
}

// QueryData handles multiple queries and returns multiple responses.
// req contains the queries []DataQuery (where each query contains RefID as a unique identifier).
// The QueryDataResponse contains a map of RefID to the response for each query, and each response
// contains Frames ([]*Frame).
func (d *AppDynamicsDatasource) QueryData(ctx context.Context, req *backend.QueryDataRequest) (*backend.QueryDataResponse, error) {
	log.DefaultLogger.Info("QueryData called", "request", req)

	// create response struct
	response := backend.NewQueryDataResponse()

	// loop over queries and execute them individually.
	for _, q := range req.Queries {
		res := d.query(ctx, req.PluginContext, q)

		// save the response in a hashmap
		// based on with RefID as identifier
		response.Responses[q.RefID] = res
	}

	return response, nil
}

type selectOption struct {
	Label string `json:"label"`
	Value string `json:"value"`
}

type queryModel struct {
	QueryKind     selectOption `json:"queryKind"`
	MetricPath    string       `json:"metricPath"`
	MetricQuery   string       `json:"metricQuery"`
	EntityKind    string       `json:"entityKind"`
	EntityName    selectOption `json:"entityName"`
	WithStreaming bool         `json:"withStreaming"`
	AppOrInfra    bool         `json:"appOrInfra"` // app=true, infra=false
	ShiftBy       selectOption `json:"shiftBy"`
	LegendFormat  string       `json:"legendFormat"`
	ConsolidateTo selectOption `json:"consolidateTo"`
}

type datastoreModel struct {
	ControllerHost   string `json:"controllerHost"`
	ControllerPort   int    `json:"controllerPort"`
	ControllerSecure bool   `json:"controllerSecure"`
	Customer         string `json:"customer"`
	Username         string `json:"username"`
	UseProxy         bool   `json:"useProxy"`
	ProxyHost        string `json:"proxyHost"`
	ProxyPort        int    `json:"proxyPort"`
	ProxySecure      bool   `json:"proxySecure"`
	Password         string
}

func (d *AppDynamicsDatasource) query(_ context.Context, pCtx backend.PluginContext, query backend.DataQuery) backend.DataResponse {
	response := backend.DataResponse{}

	// Unmarshal the JSON into our queryModel.
	var qm queryModel

	response.Error = json.Unmarshal(query.JSON, &qm)
	if response.Error != nil {
		return response
	}

	dsSettings := pCtx.DataSourceInstanceSettings
	jsonDataRaw := dsSettings.JSONData
	secureJsonData := dsSettings.DecryptedSecureJSONData

	var ds datastoreModel
	response.Error = json.Unmarshal(jsonDataRaw, &ds)
	if response.Error != nil {
		return response
	}

	log.DefaultLogger.Info(fmt.Sprintf("Query From: %v To: %v\n", query.TimeRange.From, query.TimeRange.To))
	log.DefaultLogger.Info(fmt.Sprintf("Query Data: %v\n", qm))
	log.DefaultLogger.Info(fmt.Sprintf("JSON Data: %v\n", ds))
	log.DefaultLogger.Info(fmt.Sprintf("JSON Secure Data: %v\n", secureJsonData))

	ds.Password = secureJsonData["password"]

	// create data frame response.
	frame := data.NewFrame("response")

	queryKind := qm.QueryKind

	var fields []*data.Field

	switch queryKind.Value {
	case "Metric":
		log.DefaultLogger.Info(fmt.Sprintf("Getting metrics: %v\n", qm.MetricPath))
		fields = getMetric(&ds, qm.EntityName.Value, qm.MetricPath, query.TimeRange.From, query.TimeRange.To, qm.ShiftBy, qm.LegendFormat, qm.ConsolidateTo)
		log.DefaultLogger.Info(fmt.Sprintf("Got metrics: %v\n", fields))
	case "Applications":
		log.DefaultLogger.Info(fmt.Sprintf("Getting applications\n"))
		fields = getApplicationNames(&ds)
		log.DefaultLogger.Info(fmt.Sprintf("Got applications: %v\n", fields))
	case "MetricTree":
		log.DefaultLogger.Info(fmt.Sprintf("Getting metric tree\n"))
		fields = getMetricTree(&ds, qm.EntityName.Value, qm.MetricPath)
		log.DefaultLogger.Info(fmt.Sprintf("Got metric tree: %v\n", fields))
	case "MetricField":
		log.DefaultLogger.Info(fmt.Sprintf("Getting metric fields\n"))
		fields = getMetricFields(&ds, qm.EntityName.Value, qm.MetricPath)
		log.DefaultLogger.Info(fmt.Sprintf("Got metric tree: %v\n", fields))
	default: // default is asking for metrics
		fields = getMetric(&ds, qm.EntityName.Value, qm.MetricPath, query.TimeRange.From, query.TimeRange.To, qm.ShiftBy, qm.LegendFormat, qm.ConsolidateTo)
		log.DefaultLogger.Info(fmt.Sprintf("Got metrics: %v\n", fields))
	}

	frame.Fields = fields
	/*
		// add fields.
		frame.Fields = append(frame.Fields,
			data.NewField("time", nil, []time.Time{query.TimeRange.From, query.TimeRange.To}),
			data.NewField("values1", nil, []int64{10, 30}),
		)

		// If query called with streaming on then return a channel
		// to subscribe on a client-side and consume updates from a plugin.
		// Feel free to remove this if you don't need streaming for your datasource.
		if qm.WithStreaming {
			channel := live.Channel{
				Scope:     live.ScopeDatasource,
				Namespace: pCtx.DataSourceInstanceSettings.UID,
				Path:      "stream",
			}
			frame.SetMeta(&data.FrameMeta{Channel: channel.String()})
		}

		// add the frames to the response.
		response.Frames = append(response.Frames, frame)
	*/
	response.Frames = append(response.Frames, frame)
	return response
}

// CheckHealth handles health checks sent from Grafana to the plugin.
// The main use case for these health checks is the test button on the
// datasource configuration page which allows users to verify that
// a datasource is working as expected.
func (d *AppDynamicsDatasource) CheckHealth(_ context.Context, req *backend.CheckHealthRequest) (*backend.CheckHealthResult, error) {
	log.DefaultLogger.Info("CheckHealth called", "request", req)

	var status = backend.HealthStatusOk
	var message = "Data source is working"

	dsSettings := req.PluginContext.DataSourceInstanceSettings
	jsonDataRaw := dsSettings.JSONData
	// secureJsonData := dsSettings.DecryptedSecureJSONData

	var ds datastoreModel
	err := json.Unmarshal(jsonDataRaw, &ds)
	if err != nil {
		status = backend.HealthStatusError
		message = "Invalid Datastore Configuration Format"
	}
	ds.Password = dsSettings.DecryptedSecureJSONData["password"]

	return &backend.CheckHealthResult{
		Status:  status,
		Message: message,
	}, nil
}

// SubscribeStream is called when a client wants to connect to a stream. This callback
// allows sending the first message.
func (d *AppDynamicsDatasource) SubscribeStream(_ context.Context, req *backend.SubscribeStreamRequest) (*backend.SubscribeStreamResponse, error) {
	log.DefaultLogger.Info("SubscribeStream called", "request", req)

	status := backend.SubscribeStreamStatusPermissionDenied
	if req.Path == "stream" {
		// Allow subscribing only on expected path.
		status = backend.SubscribeStreamStatusOK
	}
	return &backend.SubscribeStreamResponse{
		Status: status,
	}, nil
}

// RunStream is called once for any open channel.  Results are shared with everyone
// subscribed to the same channel.
func (d *AppDynamicsDatasource) RunStream(ctx context.Context, req *backend.RunStreamRequest, sender *backend.StreamSender) error {
	log.DefaultLogger.Info("RunStream called", "request", req)

	// Create the same data frame as for query data.
	frame := data.NewFrame("response")

	// Add fields (matching the same schema used in QueryData).
	frame.Fields = append(frame.Fields,
		data.NewField("time", nil, make([]time.Time, 1)),
		data.NewField("values", nil, make([]int64, 1)),
	)

	counter := 0

	// Stream data frames periodically till stream closed by Grafana.
	for {
		select {
		case <-ctx.Done():
			log.DefaultLogger.Info("Context done, finish streaming", "path", req.Path)
			return nil
		case <-time.After(time.Second):
			// Send new data periodically.
			frame.Fields[0].Set(0, time.Now())
			frame.Fields[1].Set(0, int64(10*(counter%2+2)))

			counter++

			err := sender.SendFrame(frame, data.IncludeAll)
			if err != nil {
				log.DefaultLogger.Error("Error sending frame", "error", err)
				continue
			}
		}
	}
}

// PublishStream is called when a client sends a message to the stream.
func (d *AppDynamicsDatasource) PublishStream(_ context.Context, req *backend.PublishStreamRequest) (*backend.PublishStreamResponse, error) {
	log.DefaultLogger.Info("PublishStream called", "request", req)

	// Do not allow publishing at all.
	return &backend.PublishStreamResponse{
		Status: backend.PublishStreamStatusPermissionDenied,
	}, nil
}
