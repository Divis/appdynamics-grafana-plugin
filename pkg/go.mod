module main

go 1.16

require (
	github.com/grafana/grafana-plugin-sdk-go v0.114.0
	gitlab.com/Divis/appdynamics-rest-api v0.1.6
)
