package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"html/template"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	appdrest "gitlab.com/Divis/appdynamics-rest-api"

	"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
)

type TemplateParams struct {
	S         []string
	ShiftBy   string
	Frequency string
	FullPath  string
}

var appdClientInitted = false
var appdClient *appdrest.Client = nil

func ensureConnected(ds *datastoreModel) bool {
	if !appdClientInitted {
		var err error

		scheme := "http"
		if ds.ControllerSecure {
			scheme = "https"
		}
		appdClient, err = appdrest.NewClientProxy(
			scheme,
			ds.ControllerHost,
			ds.ControllerPort,
			ds.Username,
			ds.Password,
			ds.Customer,
			ds.UseProxy,
			ds.ProxyHost,
			ds.ProxyPort,
			ds.ProxySecure,
		)

		if err != nil {
			log.DefaultLogger.Error("Cannot connect to AppDynamics controller")
			appdClientInitted = false
		} else {
			appdClientInitted = true
		}
	}

	return appdClientInitted
}

func getApplicationNames(ds *datastoreModel) []*data.Field {
	ensureConnected(ds)

	log.DefaultLogger.Info("Getting applications")

	apps, _ := appdClient.Application.GetApplications()
	appNames := []string{}

	fields := []*data.Field{}

	for _, app := range apps {
		appNames = append(appNames, app.Name)
	}

	log.DefaultLogger.Info("Applications %v\n", appNames)

	fields = append(fields, data.NewField("applications", nil, appNames))

	return fields
}

func parseMilliTimestamp(tm int64) time.Time {
	sec := tm / 1000
	msec := tm % 1000
	return time.Unix(sec, msec*int64(time.Millisecond))
}

func getMetric(ds *datastoreModel, appIDOrName string, path string, startTime time.Time, endTime time.Time,
	shiftBy selectOption, legendFormat string, consolidateTo selectOption) []*data.Field {

	ensureConnected(ds)

	shiftByMins, _ := strconv.Atoi(shiftBy.Value)
	shiftByMetricSuffix := ""
	var legendTemplate *template.Template = nil
	var err error
	if legendFormat != "" {
		legendTemplate, err = template.New("lt").Parse(legendFormat)
		if err != nil {
			log.DefaultLogger.Info("Error parsing legend format: " + legendFormat)
			legendTemplate = nil
		}
	}

	if shiftByMins != 0 {
		startTime = startTime.Add(-time.Duration(shiftByMins) * time.Minute)
		endTime = endTime.Add(-time.Duration(shiftByMins) * time.Minute)
		shiftByMetricSuffix = " " + shiftBy.Label
		log.DefaultLogger.Info(fmt.Sprintf("Asking for time frame %s -> %s, %d", startTime, endTime, shiftByMins))
	}

	log.DefaultLogger.Info(fmt.Sprintf("Asking for Metrics %s -> %s, %s - %s", appIDOrName, path, startTime, endTime))
	metrics, err := appdClient.MetricData.GetMetricData(appIDOrName, path, false, appdrest.TimeBETWEENTIMES, 0, startTime, endTime)
	if err != nil {
		log.DefaultLogger.Info(fmt.Sprintf("Error getting metrics  %s -> %s, %s - %s", appIDOrName, path, startTime, endTime))
		return []*data.Field{}
	}
	if len(metrics) > 0 {
		frequency := metrics[0].Frequency
		if frequency != "ONE_MIN" {
			if frequency == "TEN_MIN" {
				startTime = startTime.Add(-10 * time.Minute)
				endTime = endTime.Add(2 * 10 * time.Minute)
			} else if frequency == "SIXTY_MIN" {
				startTime = startTime.Add(-60 * time.Minute)
				endTime = endTime.Add(2 * 60 * time.Minute)
			}
			// requery with longer interval
			metrics, err = appdClient.MetricData.GetMetricData(appIDOrName, path, false, appdrest.TimeBETWEENTIMES, 0, startTime, endTime)
			if err != nil {
				log.DefaultLogger.Info(fmt.Sprintf("Error getting metrics  %s -> %s, %s - %s", appIDOrName, path, startTime, endTime))
				return []*data.Field{}
			}
		}
	}

	log.DefaultLogger.Info(fmt.Sprintf("Metrics %d, %v\n%v\n", len(metrics), metrics, err))
	fields := []*data.Field{}

	timeSeries := []time.Time{}
	dataSeries := [][]int64{}
	dataSeriesNames := []string{}

	// collect all timespams we have data for
	timeSeriesMap := map[time.Time]*[]int64{}
	for _, metric := range metrics {
		values := metric.MetricValues
		for _, value := range values {
			timestamp := parseMilliTimestamp(value.StartTimeInMillis)
			ds := []int64{}
			for i := 0; i < len(metrics); i++ {
				ds = append(ds, 0)
			}
			timeSeriesMap[timestamp] = &ds
		}
	}

	for i, metric := range metrics {
		path := metric.MetricPath
		frequency := metric.Frequency // ONE_MIN, TEN_MIN, SIXTY_MIN,
		// apply template if it exists
		if legendTemplate != nil {
			pathSegments := strings.Split(path, "|")
			templateParams := TemplateParams{
				S:         pathSegments,
				ShiftBy:   shiftBy.Label,
				Frequency: frequency,
				FullPath:  path,
			}
			buf := new(bytes.Buffer)
			legendTemplate.Execute(buf, templateParams)
			path = buf.String()
		}
		values := metric.MetricValues
		log.DefaultLogger.Info(fmt.Sprintf("Metric - path: %s, frequency: %s, value: %v\n", path, frequency, values))
		dataSeriesNames = append(dataSeriesNames, path+shiftByMetricSuffix)
		dataSeries = append(dataSeries, []int64{})
		for _, value := range values {
			// currValue := value.Current
			currValue := value.Value
			timestamp := value.StartTimeInMillis
			// log.DefaultLogger.Info(fmt.Sprintf("Metric value - currValue: %d, timestamp: %v\n", currValue, timestamp))
			if i == 0 {
				timeSeries = append(timeSeries, parseMilliTimestamp(timestamp))
			}
			dataSeries[i] = append(dataSeries[i], int64(currValue))
			// ---mapping
			dataSlice, ok := timeSeriesMap[parseMilliTimestamp(timestamp)]
			if !ok {
				log.DefaultLogger.Info(fmt.Sprintf("Metric problem - map not found for timestamp: %v\n", parseMilliTimestamp(timestamp)))
				continue
			}
			(*dataSlice)[i] = int64(currValue)
			// ---mapping
		}
	}

	timestampOrderedSlice := []time.Time{}
	timestampOrderedSliceShifted := []time.Time{}
	for t := range timeSeriesMap {
		timestampOrderedSlice = append(timestampOrderedSlice, t)
		if shiftByMins != 0 {
			timestampOrderedSliceShifted = append(timestampOrderedSliceShifted, t.Add(time.Duration(shiftByMins)*time.Minute)) //remap historica data into current timeframe
		}
	}
	sort.Slice(timestampOrderedSlice, func(i, j int) bool {
		return timestampOrderedSlice[i].UnixMilli() < timestampOrderedSlice[j].UnixMilli()
	})
	if shiftByMins != 0 {
		sort.Slice(timestampOrderedSliceShifted, func(i, j int) bool {
			return timestampOrderedSliceShifted[i].UnixMilli() < timestampOrderedSliceShifted[j].UnixMilli()
		})
	}

	// log.DefaultLogger.Info(fmt.Sprintf("Metric mid - time ordered slice: %v", timestampOrderedSlice))

	if shiftByMins == 0 {
		fields = append(fields, data.NewField("time", nil, timestampOrderedSlice))
	} else {
		fields = append(fields, data.NewField("time", nil, timestampOrderedSliceShifted))
	}
	for i := range metrics {
		dataSerie := []int64{}
		for _, t := range timestampOrderedSlice {
			dataSerie = append(dataSerie, (*timeSeriesMap[t])[i])
		}
		// log.DefaultLogger.Info(fmt.Sprintf("Metric mid - adding series: %s - %v", dataSeriesNames[i], timestampOrderedSlice))
		fields = append(fields, data.NewField(dataSeriesNames[i], nil, dataSerie))
	}

	/*
		fields = append(fields, data.NewField("time", nil, timeSeries))
		for i, series := range dataSeries {
			fields = append(fields, data.NewField(dataSeriesNames[i], nil, series))
		}
	*/

	log.DefaultLogger.Info(fmt.Sprintf("Metric return - fields: %d, timestamps: %v\n", len(fields), len(timeSeries)))
	return fields
}

func getMetricTree(ds *datastoreModel, appIDOrName string, path string) []*data.Field {
	ensureConnected(ds)

	pathElements := []*data.Field{}
	paths := []string{}
	metrics := []string{}

	subPaths, _ := GetMetricHierarchyXML(ds, appIDOrName, path)
	for _, subPath := range subPaths {
		if subPath.Type == "folder" { // folder or leaf
			paths = append(paths, subPath.Name)
		} else {
			metrics = append(metrics, subPath.Name)
		}
	}
	log.DefaultLogger.Info(fmt.Sprintf("Got subPaths %v\n", subPaths))
	log.DefaultLogger.Info(fmt.Sprintf("Got paths %v\n", paths))
	log.DefaultLogger.Info(fmt.Sprintf("Got metrics %v\n", metrics))

	pathElements = append(pathElements, data.NewField("paths", nil, paths))
	// pathElements = append(pathElements, data.NewField("metrics", nil, metrics))

	return pathElements
}

func getMetricFields(ds *datastoreModel, appIDOrName string, path string) []*data.Field {
	ensureConnected(ds)

	pathElements := []*data.Field{}
	paths := []string{}
	fields := []string{}

	subPaths, _ := GetMetricHierarchyXML(ds, appIDOrName, path)
	for _, subPath := range subPaths {
		if subPath.Type == "folder" { // folder or leaf
			paths = append(paths, subPath.Name)
		} else {
			fields = append(fields, subPath.Name)
		}
	}
	log.DefaultLogger.Info(fmt.Sprintf("Got subPaths %v\n", subPaths))
	log.DefaultLogger.Info(fmt.Sprintf("Got paths %v\n", paths))
	log.DefaultLogger.Info(fmt.Sprintf("Got metrics %v\n", fields))

	pathElements = append(pathElements, data.NewField("fields", nil, fields))
	// pathElements = append(pathElements, data.NewField("metrics", nil, metrics))

	return pathElements
}

type MetricItem struct {
	Text string `xml:",chardata"`
	Type string `xml:"type"`
	Name string `xml:"name"`
}

type MetricItems struct {
	XMLName    xml.Name     `xml:"metric-items"`
	Text       string       `xml:",chardata"`
	MetricItem []MetricItem `xml:"metric-item"`
}

// GetMetricHierarchyXML needed since root of the hierarchy is only returned in XML by the controller
func GetMetricHierarchyXML(ds *datastoreModel, appIDOrName string, metricPath string) ([]*appdrest.Metric, error) {
	ensureConnected(ds)

	uri := fmt.Sprintf("controller/rest/applications/%v/metrics?", appIDOrName)

	if metricPath != "" {
		uri += fmt.Sprintf("&metric-path=%s", url.QueryEscape(metricPath))
	}

	metricItems := MetricItems{}

	log.DefaultLogger.Info(fmt.Sprintf("Asking for %v\n", uri))

	resp, err := appdClient.DoRawRequest("GET", uri, "")
	log.DefaultLogger.Info(fmt.Sprintf("Got metrics path %v %v\n", string(resp), err))
	if err != nil {
		return nil, err
	}

	err = xml.Unmarshal(resp, &metricItems)
	if err != nil {
		return nil, err
	}

	var metrics []*appdrest.Metric

	for _, metricItem := range metricItems.MetricItem {
		metrics = append(metrics, &appdrest.Metric{
			Type: metricItem.Type,
			Name: metricItem.Name,
		})
	}

	return metrics, nil
}
