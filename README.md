# AppDynamics Grafana Datasource Plugin

This is a Grafana datasource plugin for AppDynamics. It provides datasource for AppDynamics metrics with simple query builder and possibility to compare current and historical data in the same chart. 


## Building plugin

1. Create Grafana plugins directory

  ```mkdir grafana-plugins
  cd grafana-plugins
  ```

2. Clone the repo

3. Go to plugin directory

### Frontend

1. Install dependencies

   ```bash
   yarn install
   ```

2. Build plugin in development mode 

   ```bash
   yarn dev
   ```

3. Build plugin in production mode

   ```bash
   yarn build
   ```

### Backend

1. Update [Grafana plugin SDK for Go](https://grafana.com/docs/grafana/latest/developers/plugins/backend/grafana-plugin-sdk-for-go/) dependency to the latest minor version:

   ```bash
   go get -u github.com/grafana/grafana-plugin-sdk-go
   go mod tidy
   ```

2. Build backend plugin binaries for Linux, Windows and Darwin:

   ```bash
   cd pkg
   GOOS=linux GOARCH=amd64 go build -o ../dist/gpx_appdynamics_datasource_backend_linux_amd64 .
   cd ..
   ```

## Run with Grafana under docker

1. Create data directory for Grafana

Create data directory in parallel to grafana-plugins directory

  ```bash
  mkdir data
  ```
 
2. Run under docker

  ```bash
  docker run -d \
  --volume "$(pwd)/data:/var/lib/grafana" \
  --volume "$(pwd)/grafana-plugins:/var/lib/grafana/plugins" \
  --mount type=bind,source="$(pwd)/custom.ini",target="/usr/share/grafana/conf/defaults.ini" \
  -p 3000:3000 \
  --name grafana \
  grafana/grafana:latest
  ```

