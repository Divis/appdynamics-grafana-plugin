import React, { ChangeEvent, PureComponent, SyntheticEvent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { MyDataSourceOptions, MySecureJsonData } from './types';

const { SecretFormField, FormField, Switch } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<MyDataSourceOptions> {}

interface State {}

export class ConfigEditor extends PureComponent<Props, State> {
  onControllerHostChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      controllerHost: event.target.value,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onControllerPortChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      controllerPort: parseFloat(event.target.value),
    };
    onOptionsChange({ ...options, jsonData });
  };

  onControllerSecureChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      controllerSecure: event.currentTarget.checked,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      username: event.target.value,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onCustomerChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      customer: event.target.value,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onProxyHostChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      proxyHost: event.target.value,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onProxyPortChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      proxyPort: parseFloat(event.target.value),
    };
    onOptionsChange({ ...options, jsonData });
  };

  onProxySecureChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      proxySecure: event.currentTarget.checked,
    };
    onOptionsChange({ ...options, jsonData });
  };

  onUseProxyChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      useProxy: event.currentTarget.checked,
    };
    onOptionsChange({ ...options, jsonData });
  };

  // Secure field (only sent to the backend)
  onPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonData: {
        password: event.target.value,
      },
    });
  };

  onResetPassword = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        password: false,
      },
      secureJsonData: {
        ...options.secureJsonData,
        password: '',
      },
    });
  };

  render() {
    const { options } = this.props;
    const { jsonData, secureJsonFields } = options;
    const secureJsonData = (options.secureJsonData || {}) as MySecureJsonData;

    const labelWidth = 10;
    const inputWidth = 20;

    return (
      <div className="gf-form-group">
        <div className="gf-form">
          <FormField
            label="Controller Host"
            labelWidth={labelWidth}
            inputWidth={inputWidth}
            onChange={this.onControllerHostChange}
            value={jsonData.controllerHost || ''}
            placeholder="Controller Hostname / IP"
          />
        </div>

        <div className="gf-form">
          <FormField
            label="Controller Port"
            labelWidth={labelWidth}
            inputWidth={6}
            onChange={this.onControllerPortChange}
            value={jsonData.controllerPort || ''}
            placeholder="Port"
            type="number"
            step="1"
          />
          <Switch checked={jsonData.controllerSecure || false} label="SSL" onChange={this.onControllerSecureChange} />
        </div>

        <div className="gf-form">
          <FormField
            label="Customer"
            labelWidth={labelWidth}
            inputWidth={inputWidth}
            onChange={this.onCustomerChange}
            value={jsonData.customer || ''}
            placeholder="Customer"
          />
        </div>

        <div className="gf-form">
          <FormField
            label="Username"
            labelWidth={labelWidth}
            inputWidth={inputWidth}
            onChange={this.onUsernameChange}
            value={jsonData.username || ''}
            placeholder="Username"
          />
        </div>

        <div className="gf-form-inline">
          <div className="gf-form">
            <SecretFormField
              isConfigured={(secureJsonFields && secureJsonFields.password) as boolean}
              value={secureJsonData.password || ''}
              label="Password"
              placeholder="Password"
              labelWidth={labelWidth}
              inputWidth={inputWidth}
              onReset={this.onResetPassword}
              onChange={this.onPasswordChange}
            />
          </div>
        </div>

        <div className="gf-form">
          <Switch checked={jsonData.useProxy || false} label="Use Proxy" onChange={this.onUseProxyChange} />
        </div>
        {jsonData.useProxy && (
          <div className="gf-form-group">
            <div className="gf-form">
              <FormField
                label="Proxy Host"
                labelWidth={labelWidth}
                inputWidth={inputWidth}
                onChange={this.onProxyHostChange}
                value={jsonData.proxyHost || ''}
                placeholder="Proxy Host"
              />
            </div>

            <div className="gf-form">
              <FormField
                label="Proxy Port"
                labelWidth={labelWidth}
                inputWidth={6}
                onChange={this.onProxyPortChange}
                value={jsonData.proxyPort || ''}
                placeholder="Port"
                type="number"
                step="1"
              />
              <Switch checked={jsonData.proxySecure || false} label="SSL" onChange={this.onProxySecureChange} />
            </div>
          </div>
        )}
      </div>
    );
  }
}
