import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface MyQuery extends DataQuery {
  queryKind?: any; // selects the requested data type - metrics, applications,...
  metricPath?: string; // metric path in metric and metric tree queries
  metricQuery?: string;
  entityKind?: string; // if appOrInfra = true -> "Application" else infra type specific name - "Server & Infrastructure Monitoring", "Database Monitoring", etc.
  entityName?: any; // Entity name - either application name or infra type specific name - "Server & Infrastructure Monitoring", "Database Monitoring", etc.
  withStreaming?: boolean;
  appOrInfra?: boolean; // app=true, infra=false
  shiftBy?: any; // Shift in time backwards in minutes, selectable field { label:"text description", value:300} -> 5 hours backwards
  legendFormat?: string; // empty -> no chane
  consolidateTo?: any;
}

export const defaultQuery: Partial<MyQuery> = {
  withStreaming: false,
};

export interface MyVariableQuery {
  application: string;
  tier: string;
  businessTransaction: string;
}

/**
 * These are options configured for each DataSource instance.
 */
export interface MyDataSourceOptions extends DataSourceJsonData {
  controllerHost: string;
  controllerPort: number;
  controllerSecure: boolean;
  customer: string;
  username: string;
  useProxy: boolean;
  proxyHost: string;
  proxyPort: number;
  proxySecure: boolean;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface MySecureJsonData {
  password: string;
}
