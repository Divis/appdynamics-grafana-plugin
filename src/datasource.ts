import { DataSourceInstanceSettings, MetricFindValue } from '@grafana/data';
import { DataSourceWithBackend } from '@grafana/runtime';
import { MyDataSourceOptions, MyQuery, MyVariableQuery } from './types';

export class DataSource extends DataSourceWithBackend<MyQuery, MyDataSourceOptions> {
  constructor(instanceSettings: DataSourceInstanceSettings<MyDataSourceOptions>) {
    super(instanceSettings);
  }

  async metricFindQuery(query: MyVariableQuery, options?: any): Promise<MetricFindValue[]> {
    // Retrieve DataQueryResponse based on query.
    //const response = await this.fetchMetricNames(query.namespace, query.rawQuery);

    // Convert query results to a MetricFindValue[]
    //const values = response.data.map(frame => ({ text: frame.name }));

    const values = [{ text: 'a' }, { text: 'b' }, { text: 'c' }];
    return values;
  }
}
