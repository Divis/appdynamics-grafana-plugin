import { defaults } from 'lodash';

import React, { ChangeEvent, PureComponent, SyntheticEvent } from 'react';
import { Button, LegacyForms } from '@grafana/ui';
import { QueryEditorProps, SelectableValue } from '@grafana/data';
import { DataSource } from './datasource';
import { defaultQuery, MyDataSourceOptions, MyQuery } from './types';
import { getBackendSrv } from '@grafana/runtime';

const { FormField, Switch, Select } = LegacyForms;

type Props = QueryEditorProps<DataSource, MyQuery, MyDataSourceOptions>;

const infraOptions = [
  { label: 'Database', value: 'Database Monitoring' },
  { label: 'Servers', value: 'Server & Infrastructure Monitoring' },
];

type State = {
  applications: Array<SelectableValue<string>>;
  subPaths: Array<SelectableValue<string>>;
  subPath: SelectableValue<string>;
  pathStack: Array<SelectableValue<string>>;
};

const shiftByOptions = [
  { label: 'Current', value: '' + 0 },
  { label: '- 30 mins', value: '' + 30 },
  { label: '- 1 hour', value: '' + 60 },
  { label: '- 2 hours', value: '' + 2 * 60 },
  { label: '- 3 hours', value: '' + 3 * 60 },
  { label: '- 6 hours', value: '' + 6 * 60 },
  { label: '- 12 hours', value: '' + 12 * 60 },
  { label: '- 1 day', value: '' + 24 * 60 },
  { label: '- 2 days', value: '' + 2 * 24 * 60 },
  { label: '- 3 days', value: '' + 3 * 24 * 60 },
  { label: '- 1 week', value: '' + 7 * 24 * 60 },
  { label: '- 2 weeks', value: '' + 14 * 24 * 60 },
  { label: '- 4 weeks', value: '' + 28 * 24 * 60 },
  { label: '- 8 weeks', value: '' + 2 * 28 * 24 * 60 },
  { label: '- 12 weeks', value: '' + 4 * 28 * 24 * 60 },
];

/*
const consolidateToOptions = [
  { label: 'None', value: 'None'},
  { label: '10 Minutes', value: '10'},
  { label: '60 Minutes', value: '60'},
]
*/

export class QueryEditor extends PureComponent<Props> {
  state: State = {
    applications: [], // from AppD - list of applications for select
    subPaths: [], // from AppD metric tree - list of sub paths or fields for select
    pathStack: [], // current path as array
    subPath: {}, // currently selected last sub path segment
  };

  onEntityNameChange = async (selectedOption: SelectableValue<string>) => {
    const { onChange, query } = this.props;
    const subPaths = await this.loadMetricTree(selectedOption, '');
    this.setState({ ...this.state, subPath: {}, subPaths: subPaths, pathStack: [] });

    onChange({ ...query, entityName: selectedOption, metricPath: '' });
  };

  onSubPathChange = async (selectedOption: SelectableValue<string>) => {
    const { onChange, query } = this.props;
    let ps = this.state.pathStack;
    ps.push(selectedOption);
    let pathArr = ps.map(v => {
      return v.value;
    });
    let path = pathArr.join('|');
    const subPaths = await this.loadMetricTree(this.props.query.entityName, path);

    this.setState({ ...this.state, subPath: {}, subPaths: subPaths, pathStack: ps });
    onChange({ ...query, metricPath: path });
  };

  onClickPathBack = async () => {
    const { onChange, query } = this.props;
    let ps = this.state.pathStack;
    if (ps.length > 0) {
      ps.pop();
    }
    let pathArr = ps.map(v => {
      return v.value;
    });
    let path = pathArr.join('|');
    let lastSelected = {};
    if (ps.length > 0) {
      lastSelected = ps[ps.length - 1];
    }
    const subPaths = await this.loadMetricTree(this.props.query.entityName, path);

    this.setState({ ...this.state, subPath: lastSelected, subPaths: subPaths, pathStack: ps });
    onChange({ ...query, metricPath: path });
  };

  async loadMetricTree(selectedApp: SelectableValue<string>, path: string): Promise<Array<SelectableValue<string>>> {
    console.log('Selected entity name:', selectedApp);
    const app = selectedApp !== undefined ? selectedApp : this.props.query.entityName;
    const queries = {
      queries: [
        {
          queryKind: { label: 'MetricTree', value: 'MetricTree' },
          metricPath: path,
          metricQuery: this.props.query.metricQuery,
          entityKind: this.props.query.entityKind,
          entityName: app,
          refId: 'A',
          withStreaming: true,
          datasourceId: this.props.datasource.id,
          intervalMs: 1000,
          maxDataPoints: 258,
        },
        {
          queryKind: { label: 'MetricField', value: 'MetricField' },
          metricPath: path,
          metricQuery: this.props.query.metricQuery,
          entityKind: this.props.query.entityKind,
          entityName: app,
          refId: 'B',
          withStreaming: true,
          datasourceId: this.props.datasource.id,
          intervalMs: 1000,
          maxDataPoints: 258,
        },
      ],
      from: '' + this.props.range?.from.valueOf(),
      to: '' + this.props.range?.to.valueOf(),
    };

    console.log('Backend srv', getBackendSrv());
    const response = await getBackendSrv().post('api/ds/query', queries);

    console.log('Returned', response);
    const results = response.results;
    let frames = results?.A?.frames;
    var subPathList: string[] = [];
    if (frames !== undefined) {
      subPathList = frames[0]?.data?.values[0];
    }
    console.log('Backend srv - subPathList', subPathList);
    subPathList = subPathList.sort();
    let subPaths = [];
    for (let i = 0; i < subPathList.length; i++) {
      let path = {
        value: subPathList[i],
        label: subPathList[i] + ' (f)',
      };
      subPaths.push(path);
    }
    console.log('Backend srv - subPaths', subPaths);

    frames = results?.B?.frames;
    var fieldList: string[] = [];
    if (frames !== undefined) {
      fieldList = frames[0]?.data?.values[0];
    }
    console.log('Backend srv - fieldList', fieldList);
    fieldList = fieldList.sort();
    let fields = [];
    for (let i = 0; i < fieldList.length; i++) {
      let field = {
        value: fieldList[i],
        label: fieldList[i] + ' (m)',
      };
      fields.push(field);
    }
    subPaths = subPaths.concat(fields);
    console.log('Backend srv - fields', fields);
    console.log('Backend srv - all', subPaths);
    // TODO - think over how to handle paths/folders vs. fields
    return subPaths;
  }

  onShiftByChange = (selectedOption: SelectableValue<string>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, shiftBy: selectedOption });
  };

  onMetricPathChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, metricPath: event.target.value });
  };

  onMetricQueryChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, metricQuery: event.target.value });
  };

  onLegendFormatChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query } = this.props;
    onChange({ ...query, legendFormat: event.target.value });
  };

  onWithStreamingChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, withStreaming: event.currentTarget.checked });
    // executes the query
    onRunQuery();
  };

  onAppOrInfraChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, appOrInfra: event.currentTarget.checked });
    // executes the query
    onRunQuery();
  };

  async componentDidMount() {
    const queries = {
      queries: [
        {
          queryKind: { label: 'Applications', value: 'Applications' },
          metricPath: this.props.query.metricPath,
          metricQuery: this.props.query.metricQuery,
          entityKind: this.props.query.entityKind,
          entityName: this.props.query.entityName,
          refId: 'A',
          withStreaming: true,
          datasourceId: this.props.datasource.id,
          intervalMs: 1000,
          maxDataPoints: 258,
        },
      ],
      from: '' + this.props.range?.from.valueOf(),
      to: '' + this.props.range?.to.valueOf(),
    };
    // console.log('Backend srv', getBackendSrv());
    const responsePromise = getBackendSrv().post('api/ds/query', queries);
    responsePromise.then(response => {
      // console.log('Returned', response);
      const results = response.results;
      const frames = results?.A?.frames;
      var applicationList: string[] = [];
      if (frames !== undefined) {
        applicationList = frames[0]?.data?.values[0];
      }
      // console.log('Backend srv - applications', applicationList);
      applicationList = applicationList.sort();
      let applications = [];
      for (let i = 0; i < applicationList.length; i++) {
        let app = {
          value: applicationList[i],
          label: applicationList[i],
        };
        applications.push(app);
      }
      // console.log('Backend srv - applications', applications);
      this.setState({ ...this.state, applications: applications });
    });

    const subPaths = await this.loadMetricTree(
      { value: this.props.query.entityName, label: this.props.query.entityName },
      this.props.query.metricPath + ''
    );
    this.setState({ ...this.state, subPath: {}, subPaths: subPaths, pathStack: [] });
    if (this.props.query.shiftBy === undefined) {
      this.props.onChange({ ...this.props.query, shiftBy: shiftByOptions[0] });
    }
  }

  render() {
    const query = defaults(this.props.query, defaultQuery);
    const { metricPath, entityName, appOrInfra, shiftBy, legendFormat } = query;

    const labelWidth = 8;
    const inputWidth = 8;
    const appOrInfraLabel: string = appOrInfra ? 'App' : 'Infra';
    const rootOptions = appOrInfra ? this.state.applications : infraOptions;

    if (this.props.query.shiftBy === undefined) {
      this.props.onChange({ ...this.props.query, shiftBy: shiftByOptions[0] });
    }

    return (
      <div>
        <div className="gf-form">
          <Switch checked={appOrInfra || false} label={appOrInfraLabel} onChange={this.onAppOrInfraChange} />
          <Select width={inputWidth} value={entityName} options={rootOptions} onChange={this.onEntityNameChange} />
          <div className="gf-form-label">Path Select:</div>
          <Select
            width={labelWidth}
            value={this.state.subPath}
            options={this.state.subPaths}
            onChange={this.onSubPathChange}
          />
          <Button variant="primary" size="md" key="md" onClick={this.onClickPathBack}>
            <div>&lt;-</div>
          </Button>
          <div className="gf-form-label">Shift By:</div>
          <Select width={inputWidth} value={shiftBy} options={shiftByOptions} onChange={this.onShiftByChange} />
          <FormField
            labelWidth={labelWidth}
            inputWidth={inputWidth * 2}
            value={legendFormat || ''}
            label="Legend Format"
            tooltip="Legend Format f.e. {{index .S 0}} {{index .S 3}} where .S is a path segment starting by 0. There's also {{.FullPath}}, {{.ShiftBy}} and {{.Frequency}}"
            onChange={this.onLegendFormatChange}
          />
        </div>

        <div className="gf-form">
          <FormField
            labelWidth={labelWidth}
            inputWidth={0}
            value={metricPath || ''}
            label="Metric Path"
            tooltip="AppDynamics Metric Path"
            onChange={this.onMetricPathChange}
            style={{ width: '800px' }}
          />
        </div>
      </div>
    );
  }
}
