# /bin/bash

yarn dev
# mage -v

cd pkg
GOOS=linux GOARCH=amd64 go build -o ../dist/gpx_appdynamics_datasource_backend_linux_amd64 .
cd ..

docker restart grafana
